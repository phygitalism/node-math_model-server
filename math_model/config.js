module.exports = [
    {
        name: "generateRandomMatrix", 
        binPath: ".\\bin\\RandomMatrix.exe"
    },
    {
        name: "calculateSinus", 
        binPath: ".\\bin\\Sinus.exe"
    }
]