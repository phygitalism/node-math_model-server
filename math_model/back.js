var Promise = require('bluebird'); 
var config = require('./config'); 
var child_process = require('child_process'); 

module.exports = {
    
    generateRandomMatrix: function(matrixSize){
        var __funcName = 'generateRandomMatrix'; 
        var __binPath = _getBinPath(__funcName); 
        return _exeBinAsync(__binPath, matrixSize); 
    }, 

    calculateSinus: function(number){
        var __funcName = 'calculateSinus'; 
        var __binPath = _getBinPath(__funcName);
        return _exeBinAsync(__binPath, number);
    }, 

    printPrefix: function() {
        _getCommandPrefix(); 
    }
}

//Async execute .exe file 
function _exeBinAsync(binPath){
    var __command = _getCommandPrefix() + binPath; 

    for (var i = 1 ; i < arguments.length; i ++)
        __command += ' ' + arguments[i].toString(); 

    return new Promise((resolve, reject) => {
        child_process.exec(__command, (err, stdout) => {
            if (err){
                reject(err); 
            } else {
                resolve(stdout); 
            }
        });
    });
}

//get path to bin file from config 
function _getBinPath(functionName){
    var __binPath; 

    config.forEach((value) => {
        if (value.name == functionName)
            __binPath = value.binPath; 
    }); 

    return __binPath; 
}

function _getCommandPrefix(){
    var __appconfig = require('../config'); 
   return  __appconfig.command_prefix;
}