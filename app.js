var express = require('express'); 
var mathmodel = require('./math_model/back');
var Promise = require('bluebird'); 

var os = require('os'); 
var server_config = require('./config'); 

var http_server = express(); 

//configurate server 
server_config.os_type = os.type();  
if (!server_config.os_type.startsWith('Windows'))
    server_config.command_prefix = 'mono '; 

//calculate sinus of number 
http_server.get('/sinus', (req, res) => {
    var __number = req.query.number; 
    if (__number === undefined){
        res.end('Error: check number parameter');
        return; 
    }
    mathmodel.calculateSinus(__number)
        .then((sin) => {
            res.end(sin); 
        }).catch((reason) => {
            res.end('Error: check number parameter'); 
        })
});


//generate random matrix (json format)
http_server.get('/matrix', (req, res) => {
    var __matrixSize = req.query.size; 
    if (__matrixSize === undefined){
        res.end('Error: check size parameter');
        return; 
    }

    mathmodel.generateRandomMatrix(__matrixSize)
        .then((matrix) => {
            res.end(matrix); 
        }).catch((reason) => {
            res.end('Error: check size parameter');
        });
}); 

http_server.listen(process.env.PORT || 80); 
