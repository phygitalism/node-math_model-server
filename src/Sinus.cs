﻿using System;

namespace Sinus
{
	class Program
	{
		public static void Main(string[] args)
		{
			double __number = 0 ; 
			try 
			{
				__number = Convert.ToDouble(args[0]); 
			}
			catch(Exception)
			{
				//обработка ислючения преобразования в double 
			}
			finally
			{
				Console.Write(Math.Sin(__number));
			}
		}
	}
}