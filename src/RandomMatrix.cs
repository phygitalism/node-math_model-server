﻿using System;

namespace RandomMatrix
{
	class Program
	{
		public static void Main(string[] args)
		{
			var _sizeOfMatrix = Convert.ToInt32(args[0]);
			var _random = new Random(DateTime.Now.Millisecond);
			
			Console.Write("["); 
			
			for (int row = 0 ; row < _sizeOfMatrix; row ++)
			{
				Console.Write('['); 
				for (int col = 0 ; col < _sizeOfMatrix; col ++)
				{
					Console.Write(_random.Next(0, 30));
					if (col < _sizeOfMatrix - 1) 
						Console.Write(","); 
				}
				Console.Write("]");
				
				if (row < _sizeOfMatrix - 1) 
					Console.Write(","); 
			}
			Console.Write("]"); 
		}
	}
}